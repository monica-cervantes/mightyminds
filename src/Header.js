
import React from "react"
import { useTheme } from "@mui/material/styles"
import { css, jsx } from "@emotion/react"
import { Container, Link, Avatar, AppBar, Toolbar, Typography, Box, IconButton, Menu, MenuItem, Button, Tooltip, ListItemIcon } from '@mui/material'
import HelpIcon from '@mui/icons-material/Help'
import MenuIcon from '@mui/icons-material/Menu'
import Logo from './logo.png'

const pages = ['Home', 'Classes', 'Planner', 'School Data', 'Library'];

const Header = () => {
  const theme = useTheme()

  const styles = {
    header: css`
      padding: 0 30px;
      background-color: #0f2551; 
      box-shadow: '2px 2px 6px rgb(40 36 36 / 40%);
    `,
    logo: css`
      margin-right: 0;
      padding-right: 8px;
    `,
    link: css`
      position: relative;

      padding: 5px 0;
      margin: 0 12px;
      min-width: inherit;

      color: #c1c1c1;
      text-transform: none;

      &:before {
        content: '';

        position: absolute;
        bottom: 0;
        left: 0;
        right: 0;

        border: 1px solid transparent;

        transition: all .5s linear;
      }
      
      &:hover {
        color: #fff;

        background-color: transparent;

        &:before {
          border: 1px solid #244ca5;
        }
      }
    `,
    headerLeft: css`
      display: flex;
      align-items: center;
      flex-grow: 0;
    `,
    slateBlueText: css`
      color: #4b5d81;
    `,
    helpCenter: css`
      display: flex;
      align-items: center;
      padding-right: 18px;

      color: #4b5d81;
      font-size: 12px;
      line-height: 1;
      text-decoration: none;
    `,
    accountInfo: css`
      display: flex;
      flex-direction: column;
      justify-content: center;
      align-content: center;

      padding-left: 18px;

      text-align: right;
      border-left: 1px solid #7a7a7a;
    `,
    accountName: css`
      line-height: 1;
      text-transform: none;
    `,
    questionMark: css`
      padding-right: 5px;
      font-size: 15px;
    `,
    avatar: css`
      margin-bottom: 5px;
      margin-left: 14px;
      width: 28px;
      height: 28px;
      
      font-size: 15px;
      
      background-color: #8660f1;
      border-radius: 8px;
    `,
    logoWrapper: css `
      margin-right: 11px;
    `
  }

  const [anchorElNav, setAnchorElNav] = React.useState(null);
  const [anchorElUser, setAnchorElUser] = React.useState(null);

  const handleOpenNavMenu = (event) => {
    setAnchorElNav(event.currentTarget);
  };
  const handleOpenUserMenu = (event) => {
    setAnchorElUser(event.currentTarget);
  };

  const handleCloseNavMenu = () => {
    setAnchorElNav(null);
  };

  const handleCloseUserMenu = () => {
    setAnchorElUser(null);
  };

  const stringAvatar = (name) => {
    return {
      children: `${name.split(' ')[0][0]}${name.split(' ')[1][0]}`,
    };
  }

  return (
    <AppBar position="fixed" sx={styles.header}>
      <Toolbar disableGutters>

        {/************* Mobile View *************/}

        <Box sx={{ flexGrow: 1, display: { xs: 'flex', md: 'none' } }}>
          <IconButton
            size="large"
            aria-label="account of current user"
            aria-controls="menu-appbar"
            aria-haspopup="true"
            onClick={handleOpenNavMenu}
            color="inherit"
          >
            <MenuIcon />
          </IconButton>
          <Menu
            id="menu-appbar"
            anchorEl={anchorElNav}
            anchorOrigin={{
              vertical: 'bottom',
              horizontal: 'left',
            }}
            keepMounted
            transformOrigin={{
              vertical: 'top',
              horizontal: 'left',
            }}
            open={Boolean(anchorElNav)}
            onClose={handleCloseNavMenu}
            sx={{
              display: { xs: 'block', md: 'none' },
            }}
          >
            {pages.map((page) => (
              <MenuItem key={page} onClick={handleCloseNavMenu}>
                <Typography textAlign="center">{page}</Typography>
              </MenuItem>
            ))}
          </Menu>
        </Box>
        <Typography
          variant="h6"
          noWrap
          component="div"
          sx={{ flexGrow: 1, display: { xs: 'flex', md: 'none' }, lineHeight: 1 }}
        >
          <Box sx={styles.logoWrapper}>
            <img src={Logo} />
          </Box>
          MIGHTY MINDS
        </Typography>

        {/************* Desktop and Laptop View *************/}

        <Typography
          variant="h6"
          noWrap
          component="div"
          sx={{ display: { xs: 'none', md: 'flex' }, marginRight: 0, paddingRight: '11px', borderRight: '1px solid #7a7a7a', lineHeight: 1 }  }
        >
          <Box sx={styles.logoWrapper}>
            <img src={Logo} />
          </Box>
          MIGHTY MINDS
        </Typography>

        <Box sx={{ flexGrow: 1, display: { xs: 'none', md: 'flex' } }}>
          {pages.map((page) => (
            <Button
              key={page}
              onClick={handleCloseNavMenu}
              sx={styles.link}
            >
              {page}
            </Button>
          ))}
        </Box>

        <Box sx={styles.headerLeft}>
          <Link href="#" variant="body2" sx={styles.helpCenter}><HelpIcon sx={styles.questionMark} /> Help Centre</Link>
          <Box component="span" sx={styles.accountInfo}> 
            <Typography variant="overline" sx={styles.accountName}>Jasmine Finn</Typography>
            <Typography variant="caption" sx={styles.slateBlueText}>Teacher Account</Typography>
          </Box>
          <Avatar {...stringAvatar('Jasmin Finn')} sx={styles.avatar} />
        </Box>
      </Toolbar>
    </AppBar>
  );
}


export default Header;