import logo from './logo.svg';
import './App.css';
import './index.css'
import Header from './Header'
import Banner from './Banner'
import MainContent from './MainContent';

function App() {
  return (
    <div className="App">
      <Header />
      <Banner />
      <MainContent />
    </div>
  );
}

export default App;
