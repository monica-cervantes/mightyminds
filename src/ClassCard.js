import { Grid, Button, Divider, Paper, Box, Typography } from '@mui/material'
import { useTheme } from "@mui/material/styles"
import { jsx, css } from "@emotion/react"
import OfflineBoltIcon from '@mui/icons-material/OfflineBolt'
import StarIcon from '@mui/icons-material/Star'
import AddIcon from '@mui/icons-material/Add';

const ClassCard = ({classes}) => {
  const theme = useTheme()

  const styles = {
    card: css`
      padding: 15px 17px 2px;

      text-align: left;

      background: rgb(255,255,255);
      background: linear-gradient(0deg, rgba(255,255,255,0) 88%, rgba(93,193,16,0.2413340336134454) 100%);
      border: 1px solid #edeef3;
      border-top: 3px solid #5dc110;
      box-shadow: inherit;
    `,
    bolt: css`
      padding: 2px;
      margin-bottom: 0;
      margin-right: 8px;
      
      color: #fff;

      background-color: #5dc110;
      border-radius: 3px;
    `,
    cardTitle: css`
      display: flex;
      align-items: center;
      margin-bottom: 5px;

      font-weight: bold;
    `,
    cardDetails: css`
      display: flex;
      align-items: center;

      margin-bottom: 10px;
    `,
    starIcon: css`
      color: #3972f5;
      font-size: 21px;
    `,
    year: css`
      padding: 0 8px;
      margin: 0 8px;

      border-left: 1px solid #edeef3;
      border-right: 1px solid #edeef3;
    `,
    countWrapper: css`
      display: flex;
      justify-content: space-between;
      align-items: center;

      margin-bottom: 8px;
    `,
    count: css`
      padding: 0 15px;
      
      background-color: #eceef2;
      border-radius: 5px;
    `,
    add: css`
      display: flex;
      justify-content: center;
      align-items: center;
      height: 100%;

      background-color: transparent;
      box-shadow: none;
      border: 3px dashed #c1c6d0;
      box-sizing: border-box;
      cursor: pointer;
    `,
    addIcon: css`
      color: #c1c6d0;
      font-size: 50px;
    `,
    addStudent: css `
      color: #a1a9b8;
      text-transform: none;

      &:hover {
        background-color: transparent;
      }
    `
  }

  return (
    <Grid container item columns={3} spacing={2}> 
      {
        classes.map(item => (
          <Grid item xs={1}>
            <Paper sx={styles.card}>
              <Typography variant="h5" sx={styles.cardTitle}><OfflineBoltIcon sx={styles.bolt}/> {item.title}</Typography>
              <Box sx={styles.cardDetails}><StarIcon sx={styles.starIcon} /> <Typography variant="body2" sx={styles.year}>Year {item.year}</Typography> <Typography variant="body2">{item.subject}</Typography></Box>

              <Box sx={styles.countWrapper}><Typography variant="body2">Activities due this week</Typography> <Typography variant="button" sx={styles.count}>{item.activities}</Typography></Box>
              <Box sx={styles.countWrapper}><Typography variant="body2">Assign activities</Typography> <Typography variant="button" sx={styles.count}>{item.assign}</Typography></Box>
              <Box sx={styles.countWrapper}><Typography variant="body2">Class calendar</Typography> <Typography variant="button" sx={styles.count}>{item.classCalendar}</Typography></Box>
              <Divider />
              <Button variant="text" sx={styles.addStudent}>+ Add Student</Button>
            </Paper>
          </Grid>
        ))
      }
      <Grid item xs={1}>
        <Paper sx={styles.add}>
          <AddIcon sx={styles.addIcon} />
        </Paper>
      </Grid>
    </Grid>
  )
}

export default ClassCard;