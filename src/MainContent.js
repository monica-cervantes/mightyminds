import { useState } from 'react'
import { Container, Link, Button, Grid, Paper, MenuList, MenuItem, ListItemText, Divider, Typography, Box } from '@mui/material'
import ClassCard from './ClassCard'
import Sidebar from './Sidebar'
import { StylesProvider } from '@material-ui/core'
import { jsx, css } from "@emotion/react"
import { useTheme } from "@mui/material/styles"

const MainContent = () => {
  const theme = useTheme()

  const styles = {
    sectionMain: css`
      background-color: #f8f9fc;
    `,
    heading: css`
      font-weight: 700;
    `
  }

  const [classes, setClasses] = useState([
    {
      id: 1, 
      title: "12ENGA",
      year: 12,
      subject: "English",
      activities: 3,
      assign: " ",
      classCalendar: " ",
      color: " "
    },
    {
      id: 1, 
      title: "12ENGA",
      year: 12,
      subject: "English",
      activities: 3,
      assign: " ",
      classCalendar: " ",
      color: " "
    },
    {
      id: 1, 
      title: "12ENGA",
      year: 12,
      subject: "English",
      activities: 3,
      assign: " ",
      classCalendar: " ",
      color: " "
    },
    {
      id: 1, 
      title: "12ENGA",
      year: 12,
      subject: "English",
      activities: 3,
      assign: " ",
      classCalendar: " ",
      color: " "
    },
    {
      id: 1, 
      title: "12ENGA",
      year: 12,
      subject: "English",
      activities: 3,
      assign: " ",
      classCalendar: " ",
      color: " "
    }
  ])

  return (
    <Box sx={styles.sectionMain}>
      <Container>
        <Grid container spacing={4} columns={16} sx={{ marginTop: 0 }}>
          <Grid item xs={12} sx={{ marginTop: 0 }}>
            <Typography variant="h6" className="text-left" sx={styles.heading}>Here are your classes</Typography>
            <Box className="sub-content" sx={{ marginBottom: '20px' }}>
              <Typography className="text-left" variant="body1">Select a class to view this weeks's assigned activities and begin your lesson.</Typography>
              <Link href="#" sx={{ color: '#000', textDecorationColor: '#000' }}>View all classes</Link>
            </Box>
            <ClassCard classes={classes} />
          </Grid>
          <Sidebar />
        </Grid>
      </Container>
    </Box>
  )
}

export default MainContent;