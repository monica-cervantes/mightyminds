import { jsx, css } from "@emotion/react"
import { Container, ButtonGroup, Button, Typography, Box } from '@mui/material'
import KeyboardArrowDownIcon from '@mui/icons-material/KeyboardArrowDown'

import { useTheme } from "@mui/material/styles"
import Globe from './globe.png'

const Banner = () => {
  const theme = useTheme()

  const styles = {
    sectionBanner: css`
      position: relative;
      
      padding: 99px 0 35px;

      background-color: #0d234f;
    `,
    imageWrapper: css`
      position: absolute;
      bottom: 0;
      left: 40px;

      display: flex;
      align-items: baseline;

      @media (max-width: 1499px) {
        display: none;
      }
    `,
    bannerContainer: css`
      display: flex;
      align-items: center;
      justify-content: space-between;
      text-align: left;
    `,
    buttonGroup: css`
      display: flex;
    `,
    welcome: css`
      font-weight: bold;
      margin-bottom: 22px;
    `,
    activities: css `
      position: relative;

      padding-left: 12px;
      padding-right: 13px;

      &:before {
        content: '';

        position: absolute;
        top: 2px;
        left: 0;

        width: 8px;
        height: 8px;

        background-color: #3972f5;

        border-radius: 50%;
      }
    `,
    whiteButton: css`
      margin-right: 8px;

      color: #fff;
      text-transform: none;

      border: .8px solid #fff;

      &:not(:last-of-type):hover {
        border-right-color: #1976d2 !important;
      }
      
      &:not(:last-of-type) {
        border-radius: 4px !important;
        border-right-color: #fff !important;
      }
    `,
    blueButton: css`
      padding: 6px 15px;
      
      text-transform: none;

      background-color: #3972f5;

      &:not(:last-of-type) {
        border-top-left-radius: 4px !important;
        border-bottom-left-radius: 4px !important;
      }

      &:last-child {
        padding: 6px 7px;
        margin-left: 3px !important;
      }
    `
  }
  return (
    <Box className="section-banner" sx={styles.sectionBanner}>
      <Box sx={styles.imageWrapper}>
        <img src={Globe} width="116px" />
      </Box>
      <Container className="banner-container" sx={styles.bannerContainer}>
        <Box className=""> 
          <Typography className="white-text" variant="h5" sx={styles.welcome}>Welcome back, Jasmine</Typography>

          <Typography variant="overline" className="white-text">WEEK 4 ACTIVITY SUMMARY</Typography>

          <Box className="">
            <Typography variant="caption" className="white-text" sx={styles.activities}>Due this week: 330</Typography> <Typography variant="caption" className="white-text" sx={styles.activities}>Completed: 240</Typography> <Typography variant="caption" className="white-text" sx={styles.activities}>Overdue: 33</Typography>
          </Box>
        </Box>

        <ButtonGroup>
          <Button sx={styles.whiteButton}>My Calendar</Button>
          <Button sx={styles.whiteButton}>Weekly Report</Button>
          <Button variant="contained" sx={styles.blueButton}>+ Assign Activity</Button>
          <Button variant="contained" sx={styles.blueButton}>
            <KeyboardArrowDownIcon />
          </Button>
        </ButtonGroup>
      </Container>
    </Box>
  )
}

export default Banner;