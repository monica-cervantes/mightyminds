import { Container, Link, Button, Grid, Paper, MenuList, MenuItem, ListItemText, Divider, Typography, Box } from '@mui/material'
import ArrowForwardIcon from '@mui/icons-material/ArrowForward'
import HelpIcon from '@mui/icons-material/Help'
import { jsx, css } from "@emotion/react"
import { useTheme } from "@mui/material/styles"
import Explore from "./explore.png"

const HelpAndSupportLink1= ["Visit help centre", "Send us your feedback", "Make a request or suggestion", "Report an issue"]
const HelpAndSupportLink2= ["Teacher support group", "Schedule a consultation"]

const Sidebar = () => {
  const theme = useTheme()

  const styles = {
    exploreCard: css`
      display: flex;
      flex-direction: column;

      padding: 17px 20px 5px;
      margin-top: 75px;
      margin-bottom: 20px;

      background-color: #d2dfff;
      box-shadow: inherit;
    `,
    explore: css`
      margin-bottom: 11px;

      font-size: 17px;
      font-weight: bold;  
      text-transform: none;
    `,
    exploreSub: css`
      margin-bottom: 20px;

      font-size: 15px;
    `,
    imageWrapper : css`
      margin-top: -38px;
      margin-bottom: 12px;
    `,
    blueButton: css`
      width: 100%;
      margin-bottom: 2px;

      font-weight: 700;
      text-transform: none;
    `,
    gettingStarted: css`
      font-size: 14px;  
      text-align: center;
      text-decoration: underline;
      text-transform: none;
    `,
    helpAndSupport: css`
      padding: 17px;
      margin-bottom: 50px;

      border: 1px solid #edeef3;
      box-shadow: inherit;
    `,
    links: css`
      display: flex;
      align-items: center;
    `,
    arrowIcon: css`
      margin-right: 11px;
      
      font-size: 19px;
      color: #9fabc4;
    `,
    helpAndSupportTitle: css`
      display: flex;
      align-items: center;

      margin-bottom: 13px;

      font-weight: 700;
    `,
    helpIcon: css`
      margin-right: 8px;

      color: #9fabc4;
      font-size: 15px;
    `,
    menuItem: css`
      padding: 6px 0;
    `
  }

  return (
    <Grid item xs={4}>
      <Paper sx={styles.exploreCard}>
        <Box sx={styles.imageWrapper}>
          <img src={Explore} />
        </Box>
        <Typography variant="h6" sx={styles.explore}>Explore your new portal</Typography>
        <Typography variant="body2" sx={styles.exploreSub}>Improve class focus, unit plans, live lessons, additional tracking features and much more</Typography>
        <Button variant="contained" sx={styles.blueButton}>See how it works</Button>
        <Button variant="text" sx={styles.gettingStarted}>Getting started guide</Button>
      </Paper>
      <Paper sx={styles.helpAndSupport}>
        <Typography sx={styles.helpAndSupportTitle}><HelpIcon sx={styles.helpIcon} /> Help & support</Typography>

        <Divider /> 

        <MenuList>
          {
            HelpAndSupportLink1.map(item => (
              <MenuItem sx={styles.menuItem}>
                <Typography variant="body2" sx={styles.links}><ArrowForwardIcon sx={styles.arrowIcon} /> {item}</Typography>
              </MenuItem>
            ))
          }

          <Divider /> 

          {
            HelpAndSupportLink2.map(item => (
              <MenuItem sx={styles.menuItem}>
                <Typography variant="body2" sx={styles.links}><ArrowForwardIcon sx={styles.arrowIcon} /> {item}</Typography>
              </MenuItem>
            ))
          }
        </MenuList>
      </Paper>
    </Grid>
  )
}

export default Sidebar;